import ru.nsu.ccfit.ermakova.ChatClient.ChatClient;

import javax.swing.*;

public class ChatApp {
    public static void main(String args[]) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ChatClient();
            }
        });
    }
}
